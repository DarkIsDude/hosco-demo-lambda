# hosco-demo-lambda

Welcome to this lambda tuto ! A lambda is like a function that we can call everytime we need...

## Create one

Let's create one ! We'll use NodeJS 12 and we'll use the kinesis-to-microservice role.

The create a folder on your computer and run `yarn init`. I'll let you answer alone !

We can now create an index.js and add this in our package.json:

```json
  "scripts": {
    "zip": "zip -r archive.zip src node_modules package.json"
  }
```

Inside the index.js we will put just an hello world.

```js
exports.handler = async (event) => {
    console.info(event)

    return `Mother fucker it's my first lambda !`
}
```

## Discover lambda UI

We can run a test... The test itself is only for you, we can't share test... So put your test inside the repo.

We can change the default handler if we want, change the timeout, ... We have a lot of control.

We'll now deploy our new lambda with :

- `yarn init`
- `aws lambda update-function-code --function-name <function_name> --region eu-central-1 --zip-file fileb://archive.zip`

If I run my test again, it fails ? Why ? 🖕

## Complexity

Go to hosco.slack.com > Administration > Manage apps > Custom Integration > Incoming webhooks > Add to slack.

Channel : YOU
Copy Paste the webhooks URL
Change the description, the image, the custom name (this is very important to identify your webhooks) and the icon.

Now we'll use axios to post a query: `yarn add axios` and then we can put that :

```js
const axios = require('axios')

const SLACK_URL = 'https://hooks.slack.com/services/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'

exports.handler = async (event) => {
    console.info('My event', event)

    axios.post(SLACK_URL, { text: 'What the fuck bro' })
        .then((response) => console.log('response', response))
        .catch((error) => console.log('error', error));

    return `Mother fucker it's my first lambda !`
}
```

We can now deploy. But with that, sometimes we received the message, sometimes not... Why ? 🚀

## Connect with our platform

Now we want to add the sandbox deployed on sandbox, let's do that: [status]('https://sandbox.hosco.dev/status/')

```js
const axios = require('axios')

const SLACK_URL = 'https://hooks.slack.com/services/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'
const SANDBOX_URL = 'https://sandbox.hosco.dev/status/'

exports.handler = async (event) => {
    console.info('My event', event)

    const status = await axios.get(SANDBOX_URL)
    console.info(await axios.post(SLACK_URL, { text: `What the fuck bro ${status.data.version}` }))

    return `Mother fucker it's my first lambda !`
}
```

Let's deploy that... But timeout ? Why ?

Good, we can now add a env vars and put that in the code... We can now deploy !

## Finish the configuration

We can now add a trigger and add all events inside the message...

```js
    const string = events.Records.map(event => {
        let data = JSON.parse(buffer.Buffer.from(event.kinesis.data, "base64"))
        return `data.type / `
    })
```

But now we can't debug... Because our test event is not a good one... So let's go, we'll create a new one.

We can now check logs in CloudWatch and monitoring !

## And now

You can use winston to manage log in your lambda. The logging is very important because you can't add a debugger...

We have metrics too, we should implement a dashboard to monitor [that](http://grafana.hosco.pro/d/jKy1CeIZz/external-lighthouse?orgId=1).

## Clean up

Remove your lambda and your webhooks integration.
