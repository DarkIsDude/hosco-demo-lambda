const axios = require('axios')
const buffer = require('buffer')

const SLACK_URL = 'https://hooks.slack.com/services/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'
const SANDBOX_URL = 'https://sandbox.hosco.dev/status/'

exports.handler = async (events) => {
    console.info('My event', events)
    const string = events.Records.map(event => {
        let data = JSON.parse(buffer.Buffer.from(event.kinesis.data, "base64"))
        return `${data.type} / `
    })

    const status = await axios.get(SANDBOX_URL)
    console.info(await axios.post(SLACK_URL, { text: `What the fuck bro ${status.data.version} / ${process.env.MY_ENV_VAR} / ${string}` }))

    return `Mother fucker it's my first lambda !`
}
